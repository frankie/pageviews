package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

// PerArticleResponse represents the API resultset	.
type PerArticleResponse struct {
	Items []PerArticle `json:"items"`
}

// PerArticle represents an entry in the API resultset.
type PerArticle struct {
	Project     string `json:"project"`
	Article     string `json:"article"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Access      string `json:"access"`
	Agent       string `json:"agent"`
	Views       int    `json:"views"`
}

// PerArticleHandler is an HTTP handler for per-article API requests.
type PerArticleHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *PerArticleHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var response = PerArticleResponse{Items: make([]PerArticle, 0)}

	// Parameters
	var access = strings.ToLower(params.ByName("access"))
	var agent = strings.ToLower(params.ByName("agent"))
	var article = params.ByName("article")
	var granularity = strings.ToLower(params.ByName("granularity"))
	var project = strings.TrimSuffix(strings.ToLower(params.ByName("project")), ".org")
	var start, end string

	aggregate, err := aggregateAttribute(access, agent)
	if err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	// Parameter validation
	if granularity != "daily" && granularity != "monthly" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid granularity"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if start, err = aqsassist.ValidateTimestamp(params.ByName("start")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid start timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if end, err = aqsassist.ValidateTimestamp(params.ByName("end")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid end timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	ctx := context.Background()

	query := fmt.Sprintf(`SELECT %s, timestamp FROM "local_group_default_T_pageviews_per_article_flat".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND article = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`, aggregate)
	scanner := s.session.Query(query, project, article, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var views int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&views, &timestamp); err != nil {
			s.logger.Request(r).Log(logger.ERROR, "Query failed: %s", err)
			problem.New(
				problem.Type("about:blank"),
				problem.Title(http.StatusText(http.StatusInternalServerError)),
				problem.Custom("method", http.MethodGet),
				problem.Status(http.StatusInternalServerError),
				problem.Detail(err.Error()),
				problem.Custom("uri", r.RequestURI)).WriteTo(w)
		}
		response.Items = append(response.Items, PerArticle{
			Project:     project,
			Article:     article,
			Granularity: granularity,
			Timestamp:   timestamp,
			Access:      access,
			Agent:       agent,
			Views:       views,
		})
	}

	if err := scanner.Err(); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Error querying database: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Write(data)
}

// FIXME: This probably needs a more sensible name...
func aggregateAttribute(access, agent string) (string, error) {
	switch access {
	case "all-access":
		switch agent {
		case "all-agents":
			return "aa", nil
		case "automated":
			return "ab", nil
		case "spider":
			return "as", nil
		case "user":
			return "au", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "desktop":
		switch agent {
		case "all-agents":
			return "da", nil
		case "automated":
			return "db", nil
		case "spider":
			return "ds", nil
		case "user":
			return "du", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "mobile-app":
		switch agent {
		case "all-agents":
			return "maa", nil
		case "automated":
			return "mab", nil
		case "spider":
			return "mas", nil
		case "user":
			return "mau", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "mobile-web":
		switch agent {
		case "all-agents":
			return "mwa", nil
		case "automated":
			return "mwb", nil
		case "spider":
			return "mws", nil
		case "user":
			return "mwu", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	default:
		return "", fmt.Errorf("Invalid access string")
	}
}
