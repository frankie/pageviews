package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTopPerCountry(t *testing.T) {

	t.Run("returns 200 for valid request", func(t *testing.T) {
		res, err := http.Get(apiTestURL("top-per-country/GB/all-access/2021/01/01"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching TopByCountryResponse
		// so unmarshal it to create an object of type TopByCountryResponse.  Error-out if that fails.
		obj := TopByCountryResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 1, "Invalid number of results")
	})

	// Request with an invalid date(should fail with return status code 400)
	t.Run("invalid date", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("top-per-country/GB/all-access/2021/14/01"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")
	})

}
