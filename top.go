package main

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

// TopResponse represents the API resultset.
type TopResponse struct {
	Items []Top `json:"items"`
}

// Top represents an entry in the API resultset.
type Top struct {
	Project  string    `json:"project"`
	Access   string    `json:"access"`
	Year     string    `json:"year"`
	Month    string    `json:"month"`
	Day      string    `json:"day"`
	Articles []Article `json:"articles"`
}

// Article represents article data in a Top result.
type Article struct {
	Article string `json:"article"`
	Views   int    `json:"views"`
	Rank    int    `json:"rank"`
}

// TopHandler is an HTTP handler for top API requests.
type TopHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *TopHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var articles = []Article{}
	var response = TopResponse{Items: make([]Top, 0)}

	project := strings.ToLower(strings.TrimSuffix(params.ByName("project"), ".org"))
	access := strings.ToLower(params.ByName("access"))
	year := params.ByName("year")
	month := params.ByName("month")
	day := params.ByName("day")

	ctx := context.Background()

	query := `SELECT "articlesJSON" FROM "local_group_default_T_top_pageviews".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND year = ? AND month = ? AND day = ?`
	scanner := s.session.Query(query, project, access, year, month, day).WithContext(ctx).Iter().Scanner()
	var articlesJSON string

	for scanner.Next() {
		if err = scanner.Scan(&articlesJSON); err != nil {
			s.logger.Request(r).Log(logger.ERROR, "Query failed: %s", err)
			problem.New(
				problem.Type("about:blank"),
				problem.Title(http.StatusText(http.StatusInternalServerError)),
				problem.Custom("method", http.MethodGet),
				problem.Status(http.StatusInternalServerError),
				problem.Detail(err.Error()),
				problem.Custom("uri", r.RequestURI)).WriteTo(w)
			return
		}
	}

	if err = json.Unmarshal([]byte(articlesJSON), &articles); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	response.Items = append(response.Items, Top{
		Project:  project,
		Access:   access,
		Year:     year,
		Month:    month,
		Day:      day,
		Articles: articles,
	})

	if err := scanner.Err(); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Error querying database: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	w.Write(data)
}
