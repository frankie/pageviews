package main

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

//PerProjectResponse represents a container for the per-project resultset.
type PerProjectResponse struct {
	Items []PerProject `json:"items"`
}

//PerProject represents one result from the per-project resultset.
type PerProject struct {
	Project     string `json:"project"`
	Access      string `json:"access"`
	Agent       string `json:"agent"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Views       int    `json:"views"`
}

//PerProjectHandler is the HTTP handler for per-project API requests.
type PerProjectHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *PerProjectHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var response = PerProjectResponse{Items: make([]PerProject, 0)}

	project := aqsassist.TrimProjectDomain(params.ByName("project"))
	access := strings.ToLower(params.ByName("access"))
	agent := strings.ToLower(params.ByName("agent"))
	granularity := strings.ToLower(params.ByName("granularity"))
	var start, end string
	if granularity != "daily" && granularity != "monthly" && granularity != "hourly" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid granularity"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if agent != "all-agents" && agent != "automated" && agent != "spider" && agent != "user" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid agent string"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if access != "all-access" && access != "desktop" && access != "mobile-app" && access != "mobile-web" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid access string"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if start, err = aqsassist.ValidateTimestamp(params.ByName("start")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if end, err = aqsassist.ValidateTimestamp(params.ByName("end")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if start, end, err = aqsassist.ValidateDuration(start, end, granularity); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	ctx := context.Background()

	query := `SELECT v, timestamp FROM "local_group_default_T_pageviews_per_project_v2".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND agent = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := s.session.Query(query, project, access, agent, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var views int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&views, &timestamp); err != nil {
			s.logger.Request(r).Log(logger.ERROR, "Query failed: %s", err)
			problem.New(
				problem.Type("about:blank"),
				problem.Title(http.StatusText(http.StatusInternalServerError)),
				problem.Custom("method", http.MethodGet),
				problem.Status(http.StatusInternalServerError),
				problem.Detail(err.Error()),
				problem.Custom("uri", r.RequestURI)).WriteTo(w)
		}
		response.Items = append(response.Items, PerProject{
			Project:     project,
			Access:      access,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Views:       views,
		})
	}

	if err := scanner.Err(); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Error querying database: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	w.Write(data)
}
