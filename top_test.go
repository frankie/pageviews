package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTop(t *testing.T) {

	// Valid request (should pass with return status code 200)
	t.Run("returns expected data when all params are valid", func(t *testing.T) {

		res, err := http.Get(apiTestURL("top/en.wikipedia/all-access/2021/01/02"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching TopResponse
		// so unmarshal it to create an object of type TopResponse.  Error-out if that fails.
		obj := TopResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 1, "Invalid number of results")

	})
}
