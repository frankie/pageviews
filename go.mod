module gitlab.wikimedia.org/eevans/aqs

go 1.15

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v0.0.0-20220322011350-df509f780b5c
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/eevans/servicelib-golang v1.0.3
	github.com/gocql/gocql v0.0.0-20211015133455-b225f9b53fa1
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.32.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/stretchr/testify v1.7.0
	gitlab.wikimedia.org/frankie/aqsassist v0.0.0-20220826183849-400a9e2422b4
	golang.org/x/sys v0.0.0-20211113001501-0c823b97ae02 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	schneider.vip/problem v1.6.0
)
