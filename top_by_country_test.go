package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTopByCountry(t *testing.T) {

	// Valid request (should pass with return status code 200)
	t.Run("returns 200 for valid request", func(t *testing.T) {

		t.Skip("Invalid query")

		res, err := http.Get(apiTestURL("top-by-country/en.wikipedia/all-access/2020/02"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching TopByCountryResponse
		// so unmarshal it to create an object of type TopByCountryResponse.  Error-out if that fails.
		obj := TopByCountryResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 2, "Invalid number of results")

	})
}
