package main

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

//TopPerCountryResponse represents the API resultset.
type TopPerCountryResponse struct {
	Items []TopPerCountry `json:"items"`
}

//TopPerCountry represents an entry in the API resultset.
type TopPerCountry struct {
	Country  string              `json:"country"`
	Access   string              `json:"access"`
	Year     string              `json:"year"`
	Month    string              `json:"month"`
	Day      string              `json:"day"`
	Articles []PerCountryArticle `json:"articles"`
}

//PerCountryArticle represents the country data for a returned article.
type PerCountryArticle struct {
	Article   string `json:"article"`
	Project   string `json:"project"`
	ViewsCeil int    `json:"views_ceil"`
	Rank      int    `json:"rank"`
}

//TopPerCountryHandler is an HTTP handler for top-per-country API requests.
type TopPerCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *TopPerCountryHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var articleSet = []PerCountryArticle{}
	var response = TopPerCountryResponse{Items: make([]TopPerCountry, 0)}

	country := params.ByName("country")
	access := strings.ToLower(params.ByName("access"))
	year := params.ByName("year")
	month := params.ByName("month")
	day := params.ByName("day")

	ctx := context.Background()

	var articlesData string

	query := `SELECT "articles" FROM "local_group_default_T_top_percountry".data WHERE "_domain" = 'analytics.wikimedia.org' AND country = ? AND access = ? AND year = ? AND month = ? AND day = ?`
	if err := s.session.Query(query, country, access, year, month, day).WithContext(ctx).Consistency(gocql.One).Scan(&articlesData); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Query failed: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if err = json.Unmarshal([]byte(articlesData), &articleSet); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	response.Items = append(response.Items, TopPerCountry{
		Country:  country,
		Access:   access,
		Year:     year,
		Month:    month,
		Day:      day,
		Articles: articleSet,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	w.Write(data)
}

